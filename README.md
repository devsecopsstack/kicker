# Kicker

## What is it?

Kicker is an interactive wizard tool that helps users generate their `.gitlab-ci.yml` file simply by specifying the characteristics of their project
(programming language(s), code analysis tools, packaging, hosting & deployment technology, acceptance tests, ...).

![Kicker screenshot](kicker-screen.png)

### How does it work?

Kicker is a single-page webapp (Angular) that loads a JSON file that describes:

* all available templates,
* with their latest published version (Git tag),
* with all jobs and configuration variables,
* all available template variants,
* all available variable presets.

This file is dynamically built in the `doc` project (scheduled pipeline), by crawling every `to-be-continuous` sub-project, looking for
`kicker.json` or `kicker-extras.json` files and aggregating them into one single JSON file.

* `kicker.json` files are meant to fully declare a template (with jobs and configuration). It should comply to [kicker-schema-1.json](kicker-schema-1.json) schema.
* `kicker-extras.json` files are meant to declare variable presets and/or template variants. It should comply to [kicker-extras-schema-1.json](kicker-extras-schema-1.json) schema.

For more information about **variable presets** and **template variants**, see [Self-managed GitLab &gt; Advanced Usage](https://to-be-continuous.gitlab.io/doc/self-managed/advanced/) doc.

## Developer Information

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.4.

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--configuration production` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
