import {Component, Input} from '@angular/core';
import {Extension, IncludeMode, Options, VersionMode} from '../kicker';
// import {MatomoTracker} from 'ngx-matomo';

@Component({
  selector: 'app-options-form',
  templateUrl: './options-form.component.html',
  styleUrls: ['./options-form.component.css']
})
export class OptionsFormComponent {
  @Input() extensions: Extension[];

  @Input() options: Options;

  keys = Object.keys;
  includeModes = IncludeMode;
  versionModes = VersionMode

  // constructor(
  //   private matomoTracker: MatomoTracker
  // ) {
  // }

  trackEvent(action: string, name?: string) {
    console.log('event', action, name);
    // this.matomoTracker.trackEvent('kicker', action, name);
  }

}
